const routes = require('next-routes');

module.exports = routes()
  .add('home', '/', 'index')
  .add('category', '/danh-muc/:slug/:id', 'category')
  .add('blog-detail', '/bai-viet/:slug/:id', 'blog-detail');
