import { createTypes } from 'reduxsauce';

export default createTypes(
  `
LATEST_ARTICLES_REQUEST
LATEST_ARTICLES_SUCCESS
LATEST_ARTICLES_FAIL
`,
  {}
);
