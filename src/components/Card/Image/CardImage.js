import React from 'react';
import PropTypes from 'prop-types';

import Image from '@/components/Image';
import styles from './CardImage.module.scss';

const CardImage = ({ title, image, thumbnail, ...otherProps }) => (
  <div className={styles['card-image']} {...otherProps}>
    <div className={styles.mask} />
    <Image className={styles.image} thumbnail={thumbnail} src={image} alt="" />
    <div className={styles.title}>{title}</div>
  </div>
);

CardImage.defaultProps = {
  image: null,
  title: '...',
  thumbnail: null,
};

CardImage.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  thumbnail: PropTypes.string,
};

export default CardImage;
