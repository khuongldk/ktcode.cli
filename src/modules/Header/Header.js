import React from 'react';
import cn from 'classnames';
import dynamic from 'next/dynamic';

import { LinkDefault } from '@/components/Link';
import { WrapperPage } from '@/components/Wrapper';

import styles from './Header.module.scss';

const Icon = dynamic(() => import('@/components/Icon'));

const Header = () => (
  <div className={cn(styles['header-wrapper'], styles.flex, styles['align-center'])}>
    <WrapperPage>
      <div className={cn(styles.header, styles.flex)}>
        <LinkDefault route="home">
          <Icon className="fas fa-bullhorn" />
          &nbsp;Quảng cáo
        </LinkDefault>
        <div className={styles.aside}>
          <LinkDefault route="home">
            <Icon className="fab fa-facebook-f" />
          </LinkDefault>
          <LinkDefault route="home">
            <Icon className="fab fa-instagram" />
          </LinkDefault>
          <LinkDefault route="home">
            <Icon className="fab fa-twitter" />
          </LinkDefault>
        </div>
      </div>
    </WrapperPage>
  </div>
);

export default Header;
