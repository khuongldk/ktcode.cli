import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import LazyLoad from 'react-lazyload';

import classes from '@/utils/classes';
import styles from './ImageDefault.module.scss';

const ImageDefault = ({ className, alt, src, thumbnail, ...otherProps }) => {
  const imgRef = useRef(null);
  const [loaded, setLoaded] = useState(false);
  const onLoad = () => {
    setLoaded(true);
  };
  useEffect(() => {
    setTimeout(onLoad, 1800);
  }, []);
  return (
    <LazyLoad offset={300}>
      <img
        className={`${classes(cn('default-image', { loaded }), styles)}${
          className ? ` ${className}` : ''
        }`}
        onLoad={onLoad}
        ref={imgRef}
        alt={alt}
        async
        src={loaded ? src : thumbnail}
        {...otherProps}
      />
    </LazyLoad>
  );
};

ImageDefault.defaultProps = {
  className: null,
  src: '/images/default-thumbnail.png',
  thumbnail: null,
};

ImageDefault.propTypes = {
  className: PropTypes.string,
  alt: PropTypes.string.isRequired,
  src: PropTypes.string,
  thumbnail: PropTypes.string,
};

export default ImageDefault;
