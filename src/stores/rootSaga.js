import { all } from 'redux-saga/effects';
import latestArticles from '@/modules/LatestArticles/Store/saga';

export default function* rootSaga() {
  yield all([latestArticles()]);
}
