import React from 'react';
import PropTypes from 'prop-types';
import getConfig from 'next/config';

import { CardTextAside } from '@/components/Card';
import Mapper from '@/components/Mapper';
import Content from '@/components/Content';

import styles from './ViewMostedArticles.module.scss';

const { publicRuntimeConfig } = getConfig();

const LatestArticles = ({ list }) => {
  return (
    <Content title="Xem nhiều nhất">
      <div className={styles['view-mosted-articles']}>
        <Mapper
          list={list}
          mapping={({ Thumbnail, Title, Description, published_at, sub_category }) => ({
            image: `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].url}`,
            thumbnail:
              Thumbnail[0].formats.thumbnail &&
              `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].formats.thumbnail?.url}`,
            title: Title,
            date: published_at,
            category: sub_category?.Title,
            intro: Description,
          })}
          component={CardTextAside}
        />
      </div>
    </Content>
  );
};

LatestArticles.defaultProps = {
  list: null,
};

LatestArticles.propTypes = { list: PropTypes.oneOfType([PropTypes.array]) };

export default LatestArticles;
