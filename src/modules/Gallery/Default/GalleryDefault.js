import React from 'react';
import PropTypes from 'prop-types';
import getConfig from 'next/config';
// import dynamic from 'next/dynamic';

import { CardImage } from '@/components/Card';
import Mapper from '@/components/Mapper';

import styles from './GalleryDefault.module.scss';

const { publicRuntimeConfig } = getConfig();

const GalleryDefault = ({ list }) => (
  <div className={styles['gallery-default']}>
    <Mapper
      list={list}
      mapping={({ Title, Thumbnail }) => ({
        title: Title,
        image: `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].url}`,
        thumbnail:
          Thumbnail[0].formats.thumbnail &&
          `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].formats.thumbnail?.url}`,
      })}
      component={CardImage}
      length={5}
    />
  </div>
);

GalleryDefault.defaultProps = {
  list: null,
};

GalleryDefault.propTypes = { list: PropTypes.oneOfType([PropTypes.array]) };

export default GalleryDefault;
