import axios from '@/utils/axios';

export const fetchHome = async () => {
  const res = await axios.get('/articles');
  return { data: res.data, status: res.status };
};

export default fetchHome;
