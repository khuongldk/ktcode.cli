import { useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { makeSelectLatestArticles } from './selectors';
import ActionCreators from './actions';

const UserStore = () => {
  const latestArticles = useSelector(makeSelectLatestArticles());
  const dispatch = useDispatch();
  const actions = useMemo(() => bindActionCreators({ ...ActionCreators }, dispatch), [dispatch]);
  return useMemo(
    () => ({
      latestArticles,
      actions,
    }),
    [actions, latestArticles]
  );
};

export default UserStore;
