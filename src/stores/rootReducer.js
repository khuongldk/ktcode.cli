import { combineReducers } from 'redux';
import latestArticles from '@/modules/LatestArticles/Store/reducer';

const rootReducer = combineReducers({ latestArticles });

export default rootReducer;
