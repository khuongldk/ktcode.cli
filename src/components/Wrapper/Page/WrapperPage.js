import React from 'react';
import PropTypes from 'prop-types';

import styles from './WrapperPage.module.scss';

const WrapperPage = ({ children }) => <div className={`${styles['wrapper-page']}`}>{children}</div>;

WrapperPage.defaultProps = {
  children: '',
};

WrapperPage.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default WrapperPage;
