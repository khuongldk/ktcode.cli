import { createSelector } from 'reselect';
import { INITIAL_STATE } from './reducer';

const selectLatestArticles = (state) => state.latestArticles || INITIAL_STATE;

const selectLoading = createSelector(selectLatestArticles, (state) => state.loading);
const makeSelectLoading = () => createSelector(selectLoading, (loading) => loading);

const selectLatestArticlesData = createSelector(
  selectLatestArticles,
  (state) => state.latestArticles
);
const makeSelectLatestArticles = () =>
  createSelector(selectLatestArticlesData, (latestArticles) => latestArticles);

export { makeSelectLoading, makeSelectLatestArticles, selectLatestArticles };
