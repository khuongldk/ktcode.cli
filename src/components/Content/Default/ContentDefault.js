import React from 'react';
import PropTypes from 'prop-types';

import styles from './ContentDefault.module.scss';

const ContentDefault = ({ children, title, onPrev, onNext }) => (
  <div className={styles['content-default']}>
    {title && (
      <div className={styles.title}>
        <div className={styles['icon-logo']} />
        <div className={styles.text}>{title}</div>
        <div className={styles.control}>
          {onPrev && (
            <div
              className={styles.prev}
              onClick={() => onPrev()}
              onKeyDown={() => {}}
              role="button"
              tabIndex="0"
            >
              <i className="fal fa-angle-left" />
            </div>
          )}
          {onNext && (
            <div
              className={styles.next}
              onClick={() => onNext()}
              onKeyDown={() => {}}
              role="button"
              tabIndex="0"
            >
              <i className="fal fa-angle-right" />
            </div>
          )}
        </div>
      </div>
    )}
    <div>{children}</div>
  </div>
);

ContentDefault.defaultProps = {
  title: null,
  onPrev: null,
  onNext: null,
};

ContentDefault.propTypes = {
  children: PropTypes.element.isRequired,
  title: PropTypes.string,
  onPrev: PropTypes.func,
  onNext: PropTypes.func,
};

export default ContentDefault;
