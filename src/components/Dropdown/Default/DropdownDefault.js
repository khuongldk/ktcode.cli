import React from 'react';
import PropTypes from 'prop-types';

import styles from './DropdownDefault.module.scss';

const DropdownDefault = ({ children, overlay }) => (
  <div className={styles['dropdown-default']}>
    <div className={styles.overlay}>{overlay}</div>
    {children}
  </div>
);

DropdownDefault.defaultProps = {
  children: <></>,
  overlay: <></>,
};

DropdownDefault.propTypes = {
  children: PropTypes.any,
  overlay: PropTypes.any,
};

export default DropdownDefault;
