import React, { useEffect, useState, useRef, useImperativeHandle, forwardRef } from 'react';
import PropTypes from 'prop-types';

import styles from './Swiper.module.scss';

const Swiper = forwardRef(({ children, breakpoints, dragChangeItem, ...otherProps }, ref) => {
  const slider = useRef(null);
  const [screen, setScreen] = useState(0);
  const [mouseDown, setMouseDown] = useState(false);
  const [curX, setCurX] = useState(0);
  const [widthDrag, setWidthGrag] = useState(0);
  const [transform, setTransform] = useState(0);
  const [widthItem, setWidthItem] = useState(0);
  const [sections, setSections] = useState([]);
  const [autoDrop, setAutoDrop] = useState(false);
  const [next, setNext] = useState(true);
  const [prev, setPrev] = useState(true);

  const handleScreen = (isScreen) => {
    let scr = null;
    if (breakpoints) {
      Object.keys(breakpoints)
        .reverse()
        .map((value) => {
          if (isScreen < Number(value)) {
            scr = breakpoints[`${value}`];
          }
          return false;
        });
    }
    return scr;
  };

  const { slidesPerView, spaceBetween } = handleScreen(screen) || otherProps;
  const initSlider = () => {
    setScreen(window.innerWidth);
    if (slider && slider.current) {
      slider.current.style.marginRight = `-${spaceBetween / 2}px`;
      slider.current.style.marginLeft = `-${spaceBetween / 2}px`;
      if (
        slider.current.childNodes &&
        Array.from(slider.current.childNodes) &&
        Array.from(slider.current.childNodes).length > 0
      ) {
        setSections([]);
        setWidthItem(slider.current.clientWidth / slidesPerView);
        Array.from(slider.current.childNodes).map((item, index) => {
          item.style.width = `${slider.current.clientWidth / slidesPerView - spaceBetween}px`;
          item.classList.add(styles['swiper-slide']);
          item.style.marginRight = `${spaceBetween / 2}px`;
          item.style.marginLeft = `${spaceBetween / 2}px`;
          item.draggable = false;
          setSections((sections) => [...sections, index]);
          return false;
        });
      }
    }
  };

  const handleDrop = (location) => {
    if (sections.length >= slidesPerView) {
      slider.current.style.transform = `translateX(${location}px)`;
      slider.current.style.transition = 'all 300ms ease 0s';
      setTransform(location);
    }
  };

  const onNext = () => {
    handleDrop(transform - widthItem);
    if (transform - widthItem <= widthItem * -(sections.length - slidesPerView)) {
      return false;
    }
    return true;
  };

  const onPrev = () => {
    handleDrop(transform + widthItem);
    if (transform + widthItem >= 0) {
      return false;
    }
    return true;
  };

  const handleAutoDrop = (location) => {
    if (sections.length > 0) {
      sections.map((value) => {
        const before = widthItem * -(value - 1);
        const affter = widthItem * -value;
        const dragLength = ((affter + before) / 10) * dragChangeItem;
        if (location > affter && location < before) {
          if (widthDrag < 0) {
            if (location <= dragLength) {
              handleDrop(affter);
              setAutoDrop(true);
            } else {
              handleDrop(before);
            }
          }
          if (widthDrag > 0) {
            if (location > affter - dragLength) {
              handleDrop(before);
              setAutoDrop(true);
            } else {
              handleDrop(affter);
            }
          }
        }
        return false;
      });
    }
  };

  const onMouseOut = () => {
    setMouseDown(false);
  };

  const onBlur = () => {};

  const onMouseUp = () => {
    setMouseDown(false);
    setCurX(0);
  };

  const onMouseDown = (e) => {
    setMouseDown(true);
    setCurX(e.clientX);
  };

  const onMouseMove = (e) => {
    if (mouseDown) {
      setWidthGrag(e.clientX - curX);
      e.target.parentNode.onclick = function f() {
        return false;
      };
    } else {
      e.target.parentNode.onclick = function f() {
        return true;
      };
    }
  };

  useEffect(() => {
    if (sections.length >= slidesPerView) {
      slider.current.style.transform = `translateX(${transform + widthDrag}px)`;
      slider.current.style.transition = 'all 0ms ease 0s';
    }
  }, [widthDrag]);

  useEffect(() => {
    if (!mouseDown) {
      setTransform(transform + widthDrag);
    }
  }, [mouseDown]);

  useEffect(() => {
    if (transform >= 0) {
      setPrev(false);
    }
    if (transform < 0) {
      setPrev(true);
    }
    if (transform > 0) {
      handleDrop(0);
    }

    if (transform < widthItem * -(sections.length - slidesPerView)) {
      handleDrop(widthItem * -(sections.length - slidesPerView));
      setAutoDrop(false);
    }

    if (autoDrop) {
      setAutoDrop(false);
    } else if (transform <= 0 && transform >= -(widthItem * (sections.length - slidesPerView))) {
      handleAutoDrop(transform);
    }
  }, [transform]);

  useEffect(() => {
    initSlider();
  }, [slidesPerView, spaceBetween]);

  const onTouchMove = (e) => {
    if (mouseDown) {
      setWidthGrag(e.touches[0].clientX - curX);
    }
  };
  const onTouchStart = (e) => {
    setMouseDown(true);
    setCurX(0);
    setCurX(e.touches[0].clientX);
  };
  const onTouchEnd = () => {
    setMouseDown(false);
  };

  useEffect(() => {
    function noDrag(event) {
      event.preventDefault();
    }
    document.addEventListener('dragstart', noDrag, true);
  }, []);

  useImperativeHandle(ref, () => ({
    slideNext: onNext,
    slidePrev: onPrev,
    next,
    prev,
    swiper: slider.current,
  }));

  return (
    <div className={styles.swiper}>
      <div
        ref={slider}
        className={styles['item-list']}
        onMouseOut={onMouseOut}
        onBlur={onBlur}
        onTouchEnd={onTouchEnd}
        onTouchStart={onTouchStart}
        onTouchMove={onTouchMove}
        onMouseDown={onMouseDown}
        onMouseUp={onMouseUp}
        onMouseMove={onMouseMove}
        role="button"
        tabIndex="0"
      >
        {children}
      </div>
    </div>
  );
});

Swiper.defaultProps = {
  slidesPerView: 1,
  spaceBetween: 0,
  dragChangeItem: 1,
  children: <></>,
  params: {},
  breakpoints: {},
};

Swiper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]),
  params: PropTypes.oneOfType([PropTypes.object]),
  slidesPerView: PropTypes.number,
  spaceBetween: PropTypes.number,
  dragChangeItem: PropTypes.number,
  breakpoints: PropTypes.oneOfType([PropTypes.object]),
};

export default Swiper;
