require('dotenv').config();
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const { DOMAIN_API, PAGINATION_SIZE } = process.env;

module.exports = withBundleAnalyzer({
  sassOptions: {
    localIdentName: '[name]__[local]___[hash:base64:5]',
  },
  rel: 'preload',
  publicRuntimeConfig: { DOMAIN_API, PAGINATION_SIZE },
});
