import { createReducer } from 'reduxsauce';

import { Types } from './actions';

export const INITIAL_STATE = {
  loading: false,
  latestArticles: [],
  error: false,
  errorMessage: null,
};

// ------

export const latestArticlesRequest = (state = INITIAL_STATE) => ({
  ...state,
  loading: true,
});

export const latestArticlesSuccess = (state = INITIAL_STATE, action) => ({
  ...state,
  latestArticles: [...state.latestArticles, ...action.data],
  loading: false,
});

export const latestArticlesFail = (state = INITIAL_STATE, action) => ({
  ...state,
  error: true,
  errorMessage: action.error,
  loading: false,
});

// ------

export const HANDLERS = {
  [Types.LATEST_ARTICLES_REQUEST]: latestArticlesRequest,
  [Types.LATEST_ARTICLES_SUCCESS]: latestArticlesSuccess,
  [Types.LATEST_ARTICLES_FAIL]: latestArticlesFail,
};

export default createReducer(INITIAL_STATE, HANDLERS);
