const overrideClass = (strClass, objStyles) => {
  const arrClass = document.querySelectorAll(`.${strClass}`);
  if (arrClass && Array.from(arrClass) && Array.from(arrClass).length > 0) {
    Array.from(arrClass).map((elm) => {
      elm.classList.add(objStyles[strClass]);
      elm.classList.remove(strClass);
      return false;
    });
  }
};

const Classes = (strClass, objStyles) => {
  let result = '';
  try {
    const arrClass = strClass.split(' ');
    if (arrClass && arrClass.length > 0) {
      arrClass.map((val, key) => {
        if (val !== '') {
          if (key > 0) {
            result += ' ';
          }
          result += objStyles[val];
        }
        return false;
      });
    }
  } catch {
    return false;
  }
  return result;
};

export { overrideClass };
export default Classes;
