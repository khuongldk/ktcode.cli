import React from 'react';
import dynamic from 'next/dynamic';
import PropTypes from 'prop-types';

import DefaultLayout from '@/layouts/Default';

const BlogDetail = dynamic(() => import('@/containers/BlogDetail'));

const BlogDetailPage = ({ menu }) => (
  <DefaultLayout menu={menu}>
    <BlogDetail />
  </DefaultLayout>
);

BlogDetailPage.propTypes = {
  menu: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default BlogDetailPage;
