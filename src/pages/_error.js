import React from 'react';
import PropType from 'prop-types';

function Error({ statusCode }) {
  return (
    <p>
      {statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client'}
    </p>
  );
}

Error.getInitialProps = ({ res, err }) => {
  let statusCode = 404;
  if (res) {
    statusCode = res.statusCode;
  } else if (err) {
    statusCode = err.statusCode;
  }
  return { statusCode };
};

Error.defaultProps = {
  statusCode: 200,
};

Error.propTypes = {
  statusCode: PropType.number,
};

export default Error;
