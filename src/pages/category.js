import React from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';

import DefaultLayout from '@/layouts/Default';

const Category = dynamic(() => import('@/containers/Category'));

const CategoryPage = ({ menu }) => (
  <DefaultLayout menu={menu}>
    <Category />
  </DefaultLayout>
);

CategoryPage.propTypes = {
  menu: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default CategoryPage;
