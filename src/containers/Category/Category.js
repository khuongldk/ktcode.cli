import React from 'react';

import styles from './Category.module.scss';

const Category = () => <div className={`${styles.category}`}>Category</div>;

export default Category;
