import { takeLatest, put, call } from 'redux-saga/effects';

import ActionCreators, { Types } from './actions';

import { fetchLatestArticles } from './services';

export function* changeLanguage(action) {
  const { page } = action;
  if (page) {
    try {
      const response = yield call(fetchLatestArticles, page);
      if (response?.status === 200) {
        yield put(ActionCreators.latestArticlesSuccess(response.data));
      } else {
        yield put(ActionCreators.latestArticlesFail('Phản hồi là: ', response?.status));
      }
    } catch (err) {
      yield put(ActionCreators.latestArticlesFail('Yêu cầu thất bại: ', err));
    }
  } else {
    yield put(ActionCreators.latestArticlesFail('Chưa truyền tham số.'));
  }
}

export default function* translateWatcher() {
  yield takeLatest(Types.LATEST_ARTICLES_REQUEST, changeLanguage);
}
