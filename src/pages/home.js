import React from 'react';
import PropTypes from 'prop-types';

import DefaultLayout from '@/layouts/Default';
import { fetchHotArticles, fetchSubMenu, fetchNumberOfArticles } from '@/services/common';
import { fetchLatestArticles } from '@/modules/LatestArticles';
import Home from '@/containers/Home';

const HomePage = ({ menu, ...otherProps }) => (
  <DefaultLayout menu={menu}>
    <Home {...otherProps} />
  </DefaultLayout>
);
HomePage.getInitialProps = async () => {
  const res = await fetchHotArticles(5);
  const subMenu = await fetchSubMenu();
  const latestArticles = await fetchLatestArticles(0);
  const viewMostedArticles = await fetchHotArticles(4);
  const total = await fetchNumberOfArticles();
  return {
    hotArticles: res.data,
    subMenu: subMenu.data.sub_categories,
    latestArticles: latestArticles.data,
    total: total.data,
    viewMostedArticles: viewMostedArticles.data,
  };
};

HomePage.defaultProps = {
  hotArticles: null,
  menu: null,
};

HomePage.propTypes = {
  hotArticles: PropTypes.oneOfType([PropTypes.array]),
  menu: PropTypes.oneOfType([PropTypes.array]),
};

export default HomePage;
