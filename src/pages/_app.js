import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import withReduxSaga from 'next-redux-saga';

import { fetchMenu } from '@/services/common';
import wrapper from '@/stores';
import '@/assets/StyleSheets/styles.scss';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    const { data } = await fetchMenu();
    pageProps.menu = data;
    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Head>
          <title>ktcode</title>
          <meta name="description" content="ktcode" />
          <link rel="shortcut icon" href="/favicon.ico" />
        </Head>
        <Component {...pageProps} />
      </>
    );
  }
}

export default wrapper.withRedux(withReduxSaga(MyApp));
