import axios from '@/utils/axios';

export const fetchMenu = async () => {
  const res = await axios.get('/categories');
  return { data: res.data, status: res.status };
};

export const fetchSubMenu = async () => {
  const res = await axios.get('/sub-menu');
  return { data: res.data, status: res.status };
};

export const fetchHotArticles = async (limit) => {
  const res = await axios.get(`/articles?_limit=${limit}&_sort=view:DESC,id:DESC`);
  return { data: res.data, status: res.status };
};

export const fetchNumberOfArticles = async () => {
  const res = await axios.get('/articles/count');
  return { data: res.data, status: res.status };
};

export default axios;
