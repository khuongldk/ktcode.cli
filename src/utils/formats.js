export const formatDate = (date) => {
  let DATE = '';
  const dateBase = date.split('T')[0];
  if (dateBase) {
    const dateArray = dateBase.split('-');
    DATE += dateArray[2];
    DATE += '/';
    DATE += dateArray[1];
    DATE += '/';
    DATE += dateArray[0];
  }
  return DATE;
};

export default formatDate;
