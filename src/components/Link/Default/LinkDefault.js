import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '@/routes';

import styles from './LinkDefault.module.scss';

const LinkDefault = ({ route, params, children, className, href, ...otherProps }) => (
  <Link route={route} params={params} href={href}>
    <a className={`${styles['link-default']} ${className}`} href={href} {...otherProps}>
      {children}
    </a>
  </Link>
);

LinkDefault.defaultProps = { route: null, className: '', href: null, params: {}, children: <></> };

LinkDefault.propTypes = {
  route: PropTypes.string,
  className: PropTypes.string,
  href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  params: PropTypes.oneOfType([PropTypes.object]),
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default LinkDefault;
