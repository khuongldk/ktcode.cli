import React from 'react';
import PropTypes from 'prop-types';
// import dynamic from 'next/dynamic';

import { WrapperPage } from '@/components/Wrapper';
import Content from '@/components/Content';
import LatestArticles from '@/modules/LatestArticles';
import ViewMostedArticles from '@/modules/ViewMostedArticles';
import GalleryDefault from '@/modules/Gallery';

import styles from './Home.module.scss';

// const GalleryDefault = dynamic(() => import('@/modules/Gallery'), { ssr: false });

const Home = ({ hotArticles, latestArticles, viewMostedArticles, subMenu, total }) => (
  <div className={`${styles.home}`}>
    <WrapperPage>
      <Content>
        <GalleryDefault list={hotArticles} />
      </Content>
      <LatestArticles list={latestArticles} categories={subMenu} total={total} />
      <ViewMostedArticles list={viewMostedArticles} />
    </WrapperPage>
  </div>
);

Home.defaultProps = {
  hotArticles: [],
  subMenu: [],
  latestArticles: [],
  viewMostedArticles: [],
  total: null,
};

Home.propTypes = {
  hotArticles: PropTypes.array,
  subMenu: PropTypes.array,
  latestArticles: PropTypes.array,
  viewMostedArticles: PropTypes.array,
  total: PropTypes.number,
};

export default Home;
