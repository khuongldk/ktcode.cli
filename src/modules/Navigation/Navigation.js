import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import { LinkDefault } from '@/components/Link';
import Mapper from '@/components/Mapper';
import DropdownDefault from '@/components/Dropdown';

import styles from './Navigation.module.scss';

const Navigation = ({ menu }) => {
  const subNavigation = (subMenu) => (
    <div className={styles['sub-navigation']}>
      <Mapper
        list={subMenu}
        component={({ Title, Slug, id }) => (
          <LinkDefault route="category" params={{ slug: Slug, id }}>
            <div className={cn(styles['sub-item'])}>{Title}</div>
          </LinkDefault>
        )}
      />
    </div>
  );
  return (
    <div className={`${styles['navigation-wrapper']}`}>
      <div className={cn(styles.navigation, styles.flex, styles['align-center'])}>
        <LinkDefault route="home">
          <div className={cn(styles.logo, styles.flex)} />
        </LinkDefault>
        <div className={cn(styles.menu, styles.flex, styles['align-center'])}>
          <Mapper
            list={menu}
            component={({ Title, sub_categories: subCategories }) => (
              <DropdownDefault overlay={subNavigation(subCategories)}>
                <div className={cn(styles['menu-item'], styles.flex, styles['align-center'])}>
                  {Title}
                </div>
              </DropdownDefault>
            )}
          />
        </div>
      </div>
    </div>
  );
};

Navigation.propTypes = {
  menu: PropTypes.oneOfType([PropTypes.array]).isRequired,
};

export default Navigation;
