import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import getConfig from 'next/config';

import Card, { CardCategory } from '@/components/Card';
import Mapper from '@/components/Mapper';
import Swiper from '@/components/Swiper';
import Content from '@/components/Content';

import styles from './LatestArticles.module.scss';

import useStore from './Store';

const { publicRuntimeConfig } = getConfig();

const params = {
  slidesPerView: 4,
  spaceBetween: 20,
};

const LatestArticles = ({ list, categories, total }) => {
  const swiperRef = useRef(null);
  const {
    actions: { latestArticlesRequest, latestArticlesSuccess },
    latestArticles,
  } = useStore();
  const [items, setItems] = useState(latestArticles);
  const [page, setPage] = useState(0);
  const onNext = () => {
    const state = swiperRef.current?.slideNext();
    return state;
  };
  const onPrev = () => {
    const state = swiperRef.current?.slidePrev();
    return state;
  };
  const viewMore = () => {
    setPage(page + 1);
  };

  useEffect(() => {
    if (list) {
      latestArticlesSuccess(list);
    }
  }, [list]);

  useEffect(() => {
    if (latestArticles) {
      setItems(latestArticles);
    }
  }, [latestArticles]);

  useEffect(() => {
    if (page > 0) {
      latestArticlesRequest(page);
    }
  }, [page]);

  return (
    <Content title="Mới nhất" onNext={onNext} onPrev={onPrev}>
      <div className={styles['latest-articles']}>
        <Swiper ref={swiperRef} {...params}>
          <Mapper
            list={categories}
            mapping={({ Title }) => ({
              title: Title,
            })}
            component={CardCategory}
          />
        </Swiper>
        <div className={styles['main']}>
          <Mapper
            list={items}
            mapping={({ Thumbnail, Title, Description, published_at, sub_category }) => ({
              image: `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].url}`,
              thumbnail:
                Thumbnail[0].formats.thumbnail &&
                `${publicRuntimeConfig.DOMAIN_API}${Thumbnail[0].formats.thumbnail?.url}`,
              title: Title,
              date: published_at,
              category: sub_category?.Title,
              intro: Description,
            })}
            component={Card}
          />
        </div>
        {items.length < total && (
          <div className={styles['view-more']} onClick={viewMore}>
            Xem thêm
          </div>
        )}
      </div>
    </Content>
  );
};

LatestArticles.defaultProps = {
  list: null,
};

LatestArticles.propTypes = { list: PropTypes.oneOfType([PropTypes.array]) };

export default LatestArticles;
