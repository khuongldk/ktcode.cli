import CardDefault from './Default';
import CardImage from './Image';
import CardCategory from './Category';
import CardTextAside from './TextAside';

export { CardImage, CardCategory, CardTextAside };
export default CardDefault;
