import React from 'react';
import PropTypes from 'prop-types';

import Image from '@/components/Image';

import { formatDate } from '@/utils/formats';

import styles from './CardTextAside.module.scss';

const CardTextAside = ({ title, thumbnail, image, date, category, intro }) => (
  <div className={styles['card-text-aside']}>
    <div className={styles.thumbnail}>
      <div className={styles['image-wp']}>
        <Image className={styles.image} thumbnail={thumbnail} src={image} alt="" />
      </div>
    </div>
    <div className={styles.info}>
      <div className={styles['info-more']}>
        {date && <div className={styles.published}>{formatDate(date)}</div>}
        {category && <div className={styles.category}>{category}</div>}
      </div>
      <div className={styles.title}>{title}</div>
      <div className={styles.intro}>{intro}</div>
    </div>
  </div>
);

CardTextAside.defaultProps = {
  image: null,
  title: '...',
  thumbnail: null,
  date: null,
  category: null,
  intro: null,
};

CardTextAside.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  thumbnail: PropTypes.string,
  date: PropTypes.string,
  category: PropTypes.string,
  intro: PropTypes.string,
};

export default CardTextAside;
