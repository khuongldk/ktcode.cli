import React from 'react';
import PropTypes from 'prop-types';

const Mapper = ({ list, component, mapping, length, from }) => {
  if (list && list.length > 0 && Array.isArray(list)) {
    const Item = component;
    if (length) {
      const maxLength = length + from;
      return list.map((val, index) => {
        if (from <= index && index < maxLength) {
          return <Item key={index} {...(mapping ? mapping(val) : val)} />;
        }
        return null;
      });
    }
    if (from !== 0) {
      return list.map((val, index) => {
        if (from <= index) {
          return <Item key={index} {...(mapping ? mapping(val) : val)} />;
        }
        return null;
      });
    }
    return list.map((val, index) => <Item key={index} {...(mapping ? mapping(val) : val)} />);
  }
  return <></>;
};

Mapper.defaultProps = {
  list: null,
  component: null,
  mapping: null,
  from: 0,
  length: null,
};

Mapper.propTypes = {
  list: PropTypes.oneOfType([PropTypes.array]),
  component: PropTypes.oneOfType([PropTypes.any]),
  mapping: PropTypes.func,
  from: PropTypes.number,
  length: PropTypes.number,
};

export default Mapper;
