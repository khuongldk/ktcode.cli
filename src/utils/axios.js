import axios from 'axios';

import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

const $axios = axios.create({
  baseURL: publicRuntimeConfig.DOMAIN_API,
});

export default $axios;
