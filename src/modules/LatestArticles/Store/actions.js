import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions(
  {
    latestArticlesRequest: ['page'],
    latestArticlesSuccess: ['data'],
    latestArticlesFail: ['error'],
  },
  {}
);

export { Types };
export default Creators;
