import React from 'react';
import PropTypes from 'prop-types';

import Image from '@/components/Image';

import { formatDate } from '@/utils/formats';

import styles from './CardDefault.module.scss';

const CardDefault = ({ title, thumbnail, image, date, category, intro }) => (
  <div className={styles['card-default']}>
    <div className={styles['image-wp']}>
      <Image className={styles.image} thumbnail={thumbnail} src={image} alt="" />
    </div>
    <div className={styles['info-more']}>
      {date && <div className={styles.published}>{formatDate(date)}</div>}
      {category && <div className={styles.category}>{category}</div>}
    </div>
    <div className={styles.title}>{title}</div>
    <div className={styles.intro}>{intro}</div>
  </div>
);

CardDefault.defaultProps = {
  image: null,
  title: '...',
  thumbnail: null,
  date: null,
  category: null,
  intro: null,
};

CardDefault.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  thumbnail: PropTypes.string,
  date: PropTypes.string,
  category: PropTypes.string,
  intro: PropTypes.string,
};

export default CardDefault;
