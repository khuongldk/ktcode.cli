import axios from '@/utils/axios';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

export const fetchLatestArticles = async (page) => {
  const res = await axios.get(
    `/articles?_start=${publicRuntimeConfig.PAGINATION_SIZE * page}&_limit=${
      publicRuntimeConfig.PAGINATION_SIZE
    }&_sort=id:DESC`
  );
  return { data: res.data, status: res.status };
};

export default fetchLatestArticles;
