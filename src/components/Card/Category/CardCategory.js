import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import styles from './CardCategory.module.scss';

const CardCategory = ({ title, ...otherProps }) => {
  const colorsLeft = [
    '#6DD5FA',
    '#7F7FD5',
    '#3566A4',
    '#1C2A58',
    '#2f80ed',
    '#4AC29A',
    '#50C9C3',
    '#1CD8D2',
    '#56CCF2',
  ];
  const colorsRight = [
    '#2980B9',
    '#86A7E3',
    '#588BBA',
    '#4B5EAA',
    '#56CCF2',
    '#bdfff3',
    '#96deda',
    '#93edc7',
    '#2f80ed',
  ];
  const cardRef = useRef(null);
  const randomNumber = (min, max) => Math.floor(Math.random() * max + min);
  useEffect(() => {
    cardRef.current.style.background = `linear-gradient(141deg, ${
      colorsLeft[randomNumber(0, 9)]
    } 49%, ${colorsRight[randomNumber(0, 9)]})`;
  }, []);
  return (
    <div className={styles['card-category']} ref={cardRef} {...otherProps}>
      {title}
    </div>
  );
};

CardCategory.defaultProps = {
  title: '...',
};

CardCategory.propTypes = {
  title: PropTypes.string,
};

export default CardCategory;
