import React from 'react';
import PropTypes from 'prop-types';

import { LinkDefault } from '@/components/Link';
import Navigation from '@/modules/Navigation';
import Header from '@/modules/Header';

import styles from './Default.module.scss';

const DefaultLayout = ({ children, menu }) => (
  <div className={styles['default-layout']}>
    <LinkDefault className={`${styles['skip-link']}`} href="#maincontent">
      Skip to main
    </LinkDefault>
    <Header />
    <Navigation menu={menu} />
    <div className={styles['main-content']} id="maincontent">
      {children}
    </div>
  </div>
);

DefaultLayout.defaultProps = {
  children: <></>,
  menu: [],
};

DefaultLayout.propTypes = {
  children: PropTypes.element,
  menu: PropTypes.oneOfType([PropTypes.array]),
};

export default DefaultLayout;
